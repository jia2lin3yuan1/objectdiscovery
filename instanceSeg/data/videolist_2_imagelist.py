import os
import os.path as osp
from glob import glob

import argparse


def str2bool(v):
    return v.lower() in ("yes", "true", "t", "1")

def parse_args(argv=None):
    parser = argparse.ArgumentParser(description='dataset config')
    parser.add_argument('--dataset', default='mcsvideo3_inter', type=str,
                        help='json scripts to overwrite partial args')
    parser.add_argument('--phase', default='train', type=str,
                        help='train|valid')
    parser.add_argument('--rm_images_class', default=[], nargs="+", type=int,
                        help='remove images that has no category in the list' )
    parser.add_argument('--rm_images_prob', default=0.97, type=int,
                        help='probability to remove images not satisfy given condition')
    parser.add_argument('--augment_distribution', default=False, type=str2bool,
                        help='do example agumentation for sampling more images with N+ objects')

    return parser.parse_args(argv)


def


if __name__ == '__main__':
    args = parse_args()

    if 'coco' in args.dataset:
        from data.config_coco import coco_dataset as cfg
    elif 'mcsvideo3_inter' in args.dataset:
        from data.config_mcsVideo3_inter import mcsvideo3_interact_dataset as cfg
    elif 'mcsvideo_inter' in args.dataset:
        from data.config_mcsVideo_inter import mcsvideo_interact_dataset as cfg
    elif 'mcsvideo3_voe' in args.dataset:
        from data.config_mcsVideo3_voe import mcsvideo_voe_dataset as cfg
    else:
        from data.config_coco import coco_dataset as cfg

    if args.phase=='train':
        base_dir = cfg.train_images
        info_file = cfg.train_info
    else:
        base_dir = cfg.valid_images
        info_file = cfg.valid_info

    # video list
    with open(info_file) as f:
        fdir_list = [x.strip() for x in f.readlines()]

    # get image list from each sub-folder.
    key = '.jpg'
    image_set_index = []
    for fdir in fdir_list:
        glob_imgs = glob(osp.join(base_dir, fdir, '*'+key))
        img_list = [osp.join(fdir, osp.basename(v).split(key)[0]) for v in glob_imgs]
        if len(self.select_semids) == 0:
            image_set_index += img_list
        else:
            for ele in img_list:
                semI = self._readLabelImage(ele, has_inst=False)
                if any([k in self.select_semids for k in np.unique(semI)]):
                    image_set_index.append(ele)
                elif np.random.random() > 0.97:
                    image_set_index.append(ele)

    return image_set_index



