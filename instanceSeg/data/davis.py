import os
import os.path as osp
import sys
import cv2
import skimage
import random
import numpy as np
from scipy import misc as smisc
from glob import glob

import torch
import torch.utils.data as data
import torch.nn.functional as F

from .base_dataset import Detection
from .base_dataset import FromImageAnnotationTransform as AnnotationTransform

def read_flo(filename):
    with open(filename, 'rb') as f:
        magic = np.fromfile(f, np.float32, count=1)
        if 202021.25 != magic:
            print('Magic number incorrect. Invalid .flo file')
            sys.exit(-1)
        else:
            w = np.fromfile(f, np.int32, count=1)
            h = np.fromfile(f, np.int32, count=1)
            data = np.fromfile(f, np.float32, count=int(2*w*h))
            # Reshape data into 3D array (columns, rows, bands)
            data2D = np.resize(data, (h[0], w[0],2))
            return data2D

def flow_to_color(flowI, sec=8):
    """Converts flow to 3-channel color image.

    Args:
        flowI: array of shape [height, width, 2].
    """
    ht, wd = flowI.shape[:2]
    flow_u, flow_v = flowI[..., 0], flowI[...,1]
    max_flow = np.max(np.abs(flowI))

    mag = np.sqrt(flow_u**2 + flow_v**2)
    angle = np.arctan2(flow_v, flow_u)

    im_h = np.mod(angle / (2 * np.pi) + 1.0, 1.0)
    im_s = np.clip(mag * sec / max_flow, 0, 1)
    im_v = np.clip(sec - im_s, 0, 1)
    im_hsv = np.stack([im_h, im_s, im_v], axis=-1)
    im = skimage.color.hsv2rgb(im_hsv)

    return im


class DAVISDetection(Detection):
    """`
    Args:
        root (string): Root directory where images are downloaded to.
        set_name (string): Name of the specific set of images.
        transform (callable, optional): A function/transform that augments the
                                        raw images`
        target_transform (callable, optional): A function/transform that takes
        in the target (semImg, instImg) and transforms it to bbox+cls.
        prep_crowds (bool): Whether or not to prepare crowds for the evaluation step.
    """

    def __init__(self, image_path, mask_out_ch=1, info_file=None, option=None,
                 transform=None, target_transform=None,
                 dataset_name='dummy', running_mode='test', model_mode='InstSeg'):
        '''
        Args:running_mode: 'train' | 'val' | 'test'
             model_mode: 'InstSeg' | 'SemSeg' | 'ObjDet'
        '''
        super(DAVISDetection, self).__init__(image_path,
                                            mask_out_ch,
                                            option.sem_weights,
                                            transform,
                                            AnnotationTransform(option),
                                            running_mode,
                                            model_mode,
                                            option.sem_fg_stCH)
        self.ignore_label  = option.ignore_label
        self.name          = dataset_name
        self.image_set     = running_mode
        self.ids           = self._load_image_set_index(info_file)
        self.running_mode  = running_mode
        self.read_motion   = option.extra_input


    def _load_image_set_index(self, info_file):
        """
        find out which indexes correspond to given image set (train or val)
        :return:
        """
        base_dir = self.root
        with open(info_file) as f:
            fdir_list = [x.strip() for x in f.readlines()]

        # get image list from each sub-folder.
        key = '_flow_bw.flo'
        image_set_index = []
        for fdir in fdir_list:
            glob_imgs = glob(osp.join(base_dir, 'Flow', fdir, '*'+key))
            img_list = [osp.join(fdir, osp.basename(v).split(key)[0]) for v in glob_imgs]
            image_set_index += img_list

        return image_set_index


    def save_subpath(self, index, result_path='', subPath=''):
        fname = self.ids[index]
        sub_folder, fname = fname.split('/')
        result_path =  osp.join(result_path, subPath, sub_folder)
        os.makedirs(osp.join(result_path), exist_ok=True)
        return {'fname': fname,
                'out_dir': result_path,
                'file_key': self.ids[index]}

    def pull_item(self, index):
        """
        Args:
            index (int): Index
        Returns:
            tuple: Tuple (image, target, masks, height, width, crowd).
                   target is the object returned by ``coco.loadAnns``.
            Note that if no crowd annotations exist, crowd will be None
        """
        fpath = self.ids[index]
        fdir, fname = fpath.split('/')

        # read image
        img_file  = osp.join(self.root, 'JPEGImages/480p', fdir, fname+'.jpg')
        img = cv2.imread(img_file)
        height, width, _ = img.shape

        if self.read_motion:
            flow_file  = osp.join(self.root, 'Flow', fdir, fname+'_flow_bw.flo')
            flowI = read_flo(flow_file)

        num_crowds = 0
        if self.has_gt:
            anns = self.pull_anno(index)
            semI, masks, target = anns['sem'], anns['inst_mask'], anns['bbox']
        else:
            semI, masks, target = None, None, None

        if target is None:
            masks  = np.zeros([1, height, width], dtype=np.float)
            target = np.array([[0,0,1,1,0]])

        if self.read_motion:
            num_crowds = 2
            masks = np.concatenate([masks, np.transpose(flowI, [2,0,1])], axis=0)
            target = np.concatenate([target, np.asarray([[0,0,1,1,-1], [0,0,1,1,-1]])], axis=0)

        # add BG semantic channels, for panoptic segmentation
        if semI is not None:
            sem_bgs =np.asarray([[0,0,1,1,0]]*self.sem_fg_stCH)
            sem_bg_maskI = np.zeros([self.sem_fg_stCH, height, width])
            for k in range(self.sem_fg_stCH):
                sem_bg_maskI[k] = (semI==k).astype(np.float)
            masks  = np.concatenate([sem_bg_maskI, masks], axis=0)
            target = np.concatenate([sem_bgs, target], axis=0)

        if self.transform is not None:
            img, masks, boxes, labels = self.transform(img, masks, target[:, :4],
                                {'num_crowds': num_crowds, 'labels': target[:, 4]})
            # num_crowds is stored inheirted from coco dataset
            num_crowds = labels['num_crowds']
            labels     = labels['labels']
            target = np.hstack((boxes, np.expand_dims(labels, axis=1)))

        # separate flowI from masks, and concatenate it to image.
        if self.read_motion:
            flowI = np.transpose(masks[-2:], [1,2,0])
            img   = np.concatenate([img, flowI], axis=-1)
            num_crowds, masks, target = 0, masks[:-2], target[:-2]

        return torch.from_numpy(img).permute(2, 0, 1), target, masks, height, width, num_crowds


    def pull_image(self, index):
        '''Returns the original image object at index in PIL form

        Note: not using self.__getitem__(), as any transformations passed in
        could mess up this functionality.

        Argument:
            index (int): index of img to show
        Return:
            dict of network input
        '''
        fpath = self.ids[index]
        fdir, ori_name = fpath.split('/')

        img_file  = osp.join(self.root, 'DAVIS-'+self.running_mode, 'JPEGImages/480p', fdir, ori_name+'.jpg')
        bgrI = cv2.imread(img_file)

        flow_file  = osp.join(self.root, 'DAVIS-'+self.running_mode, 'Flow', fdir, ori_name+'_flow_bw.flo')
        flowI = read_flo(flow_file)
        flowI = flow_to_color(flowI)

        return {'rgb': bgrI[..., [2,1,0]], 'flow': flowI}


    def pull_anno(self, index):
        '''Returns the original annotation of image at index

        Note: not using self.__getitem__(), as any transformations passed in
        could mess up this functionality.

        Argument:
            index (int): index of img to get annotation of
        Return:
            list:  [img_id, [[bbox coords, label],...]]
                eg: ('001718', [('dog', (96, 13, 438, 332))])
        '''
        fpath = self.ids[index]
        fdir, ori_name = fpath.split('/')
        inst_file      = osp.join(self.root, 'Annotations/480p', fdir, ori_name+'.png')
        instI          = smisc.imread(inst_file, mode='P')
        semI           = (instI>0).astype(np.uint8)
        height, width  = semI.shape[:2]

        # obtain bbox and mask
        if self.target_transform is not None:
            trans_src = [semI, instI]
            target = self.target_transform(trans_src, width, height)
            target = np.array(target)
            if len(target) == 0:
                target, masks = None, None
            else:
                # instance binary masks in different channels
                cor_instI = trans_src[1]
                masks = np.eye(cor_instI.max()+1)[cor_instI] # [ht, wd, ch]
                eff_chs = [0] + [ele for ele in np.unique(cor_instI) if ele > 0]
                masks = masks[..., eff_chs]
                masks = np.transpose(masks[:,:,1:], [2,0,1]).astype(np.float32)
        else:
            target, masks = None, None

        return {'bbox': target,
                'inst_mask': masks,
                'sem': semI,
                'inst': instI}

