import os
import os.path as osp
import sys
import cv2
import random
import numpy as np
from scipy import misc as smisc
from glob import glob

import torch
import torch.utils.data as data
import torch.nn.functional as F

from .base_dataset import Detection
from .base_dataset import FromImageAnnotationTransform as AnnotationTransform

class CityscapesDetection(Detection):
    """`
    Args:
        root (string): Root directory where images are downloaded to.
        set_name (string): Name of the specific set of Cityscapes images.
        transform (callable, optional): A function/transform that augments the
                                        raw images`
        target_transform (callable, optional): A function/transform that takes
        in the target (semImg, instImg) and transforms it to bbox+cls.
        prep_crowds (bool): Whether or not to prepare crowds for the evaluation step.
    """

    def __init__(self, image_path, mask_out_ch=1, info_file=None, option=None,
                 transform=None, target_transform=None,
                 dataset_name='cityscapes', running_mode='test', model_mode='InstSeg'):
        '''
        Args:running_mode: 'train' | 'val' | 'test'
             model_mode: 'InstSeg' | 'SemSeg' | 'ObjDet'
        '''
        super(CityscapesDetection, self).__init__(image_path,
                                            mask_out_ch,
                                            option.sem_weights,
                                            transform,
                                            AnnotationTransform(option),
                                            running_mode,
                                            model_mode,
                                            option.sem_fg_stCH)

        self.ignore_label   = option.ignore_label
        self.name           = dataset_name
        self.data_type      = option.data_type
        self.image_set      = running_mode

        self.ids           = self._load_image_set_index()
        self.parse_sem     = option.parse_sem
        self.parse_sem_lut = option.label_map


    def _load_image_set_index(self):
        """
        find out which indexes correspond to given image set (train or val)
        :return:
        """
        base_dir = osp.join(self.root, 'Code/gt'+self.data_type, self.image_set)
        fdir_list = os.listdir(base_dir)

        # get image list from each sub-folder.
        key = '_gt'+self.data_type+'_labelTrainIds.png'
        image_set_index = []
        for fdir in fdir_list:
            glob_imgs = glob(osp.join(base_dir, fdir, '*'+key))
            img_list = [osp.join(fdir, osp.basename(v).split(key)[0]) for v in glob_imgs]
            image_set_index += img_list

        return image_set_index

    def _instanceImg_parsing(self, instI):
        '''
        in the original output '*_instanceTrainIds.png',
            if has instance.
                the instance label = labelsTrainId*1000 + inst_id_in_categories
            else
                                   = labelsTrainId.
           labelsTrainIds is referring to the './code/cityscapesscripts/helper/labels.py'
        this function re-order the instances label as 1~N-1. N is the # of valid instances
        '''
        instI[instI<1000] = 0
        val_list = np.unique(instI)

        rpl_dict, cnt = dict(), 1
        for val in val_list:
            if val != 0:
                rpl_dict[val] = cnt
                cnt = cnt + 1
            else:
                rpl_dict[0] = 0
        instI = np.vectorize(rpl_dict.get)(instI)
        return instI

    def _semanticImg_parsing(self, semI):
        '''
        @func: for instanceSeg, some categories are hided as 0.
        '''
        if len(semI.shape) ==2:
            semI = np.vectorize(self.parse_sem_lut.get)(semI)
        else:
            semIds = np.unique(np.argmax(semI, axis=-1))
            for sid in semIds:
                if sid not in self.parse_sem_lut:
                    semI[semI==sid,   0] += semI[semI==sid, sid]
                    semI[semI==sid, sid]  = 0
        return semI

    def save_subpath(self, index, result_path='', subPath=''):
        fname = self.ids[index]
        sub_folder, fname = fname.split('/')
        result_path =  osp.join(result_path, subPath, sub_folder)
        os.makedirs(osp.join(result_path), exist_ok=True)
        return {'fname': fname,
                'out_dir': result_path,
                'file_key': self.ids[index]}

    def pull_item(self, index):
        """
        Args:
            index (int): Index
        Returns:
            tuple: Tuple (image, target, masks, height, width, crowd).
                   target is the object returned by ``coco.loadAnns``.
            Note that if no crowd annotations exist, crowd will be None
        """
        fpath = self.ids[index]
        sub_folder, fname = fpath.split('/')

        # read image
        image_file = osp.join(self.root, 'leftImg8bit', self.image_set, sub_folder,
                                                    fname + '_leftImg8bit.png')
        img = cv2.imread(image_file)
        height, width, _ = img.shape

        num_crowds = 0
        if self.has_gt:
            anns = self.pull_anno(index)
            semI, masks, target = anns['sem'], anns['inst_mask'], anns['bbox']
        else:
            semI, masks, targets = None, None, None

        if target is None:
            masks  = np.zeros([1, height, width], dtype=np.float)
            target = np.array([[0,0,1,1,0]])

        # add BG semantic channels, for panoptic segmentation
        if semI is not None:
            sem_bgs =np.asarray([[0,0,1,1,0]]*self.sem_fg_stCH)
            sem_bg_maskI = np.zeros([self.sem_fg_stCH, height, width])
            for k in range(self.sem_fg_stCH):
                sem_bg_maskI[k] = (semI==k).astype(np.float)
            masks  = np.concatenate([sem_bg_maskI, masks], axis=0)
            target = np.concatenate([sem_bgs, target], axis=0)

        # transform for augmentation
        if self.transform is not None:
            img, masks, boxes, labels = self.transform(img, masks, target[:, :4],
                                    {'num_crowds': num_crowds, 'labels': target[:, 4]})

            # num_crowds is stored inheirted from coco dataset
            num_crowds = labels['num_crowds']
            labels     = labels['labels']
            target = np.hstack((boxes, np.expand_dims(labels, axis=1)))

        return torch.from_numpy(img).permute(2, 0, 1), target, masks, height, width, num_crowds


    def pull_image(self, index):
        '''Returns the original image object at index in PIL form

        Note: not using self.__getitem__(), as any transformations passed in
        could mess up this functionality.

        Argument:
            index (int): index of img to show
        Return:
            dict of network input
        '''
        fpath = self.ids[index]
        sub_folder, fname = fpath.split('/')
        image_file = osp.join(self.root, 'leftImg8bit', self.image_set, sub_folder,
                                                    fname + '_leftImg8bit.png')
        bgrI = cv2.imread(image_file)
        return {'rgb': bgrI[:, :, [2,1,0]]}

    def pull_anno(self, index):
        '''Returns the original annotation of image at index

        Note: not using self.__getitem__(), as any transformations passed in
        could mess up this functionality.

        Argument:
            index (int): index of img to get annotation of
        Return:
            dict of annotations -- bbox: list of bbox in [x0,y0,x1,y1,clsId]
                                   inst_mask: object mask in array [N, ht, wd]
                                   sem: sem label image in [ht, wd]
                                   inst: inst label image in [ht, wd]
        '''
        fpath = self.ids[index]
        sub_folder, fname = fpath.split('/')

        ann_path  = osp.join(self.root,'Code/gt'+self.data_type, self.image_set)
        pfx_key   = '_gt'+self.data_type
        sem_file  = osp.join(ann_path, sub_folder, fname+pfx_key+'_labelTrainIds.png')
        semI      = smisc.imread(sem_file, mode='P')
        inst_file = osp.join(ann_path, sub_folder, fname+pfx_key+'_instanceTrainIds.png')
        instI     = smisc.imread(inst_file, mode='I')
        height, width = semI.shape[:2]

        if self.parse_sem:
            semI = self._semanticImg_parsing(semI)
        instI = self._instanceImg_parsing(instI)

        # obtain bbox and mask
        if self.target_transform is not None:
            trans_src = [semI, instI]
            target = self.target_transform(trans_src, width, height)
            target = np.array(target)
            if len(target) == 0:
                target, masks = None, None
            else:
                # instance binary masks in different channels
                cor_instI = trans_src[1]
                masks = np.eye(cor_instI.max()+1)[cor_instI]
                eff_chs = [0] + [ele for ele in np.unique(cor_instI) if ele > 0]
                masks = masks[..., eff_chs]
                masks = np.transpose(masks[:,:,1:], [2,0,1]).astype(np.float)
        else:
            target, masks = None, None

        return {'bbox': target,
                'inst_mask': masks,
                'sem': semI,
                'inst': instI}

