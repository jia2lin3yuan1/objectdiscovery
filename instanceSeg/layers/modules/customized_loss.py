################################################################
#####  instance network loss
################################################################

import torch
from torch.nn import functional as F

from layers.modules.lib_python import create_pairwise_conv_kernel
from layers.modules.lib_python import create_slope_plane

import pdb

def create_global_slope_plane(ht, wd):
    '''
    return tensor in shape [1, 1, ht, wd]
    '''
    plane = create_slope_plane(ht, wd)
    return torch.FloatTensor(plane)


def adjust_smooth_l1_loss(y_pred, theta=0.1):
    # small gradient when close to 0, constant gradient in large value zone
    less_grad_factor = 1./(2*theta)
    less_loss_bias   = less_grad_factor * theta**2
    less_than_theta  = (y_pred < theta).float()
    loss = (less_than_theta*y_pred**2*less_grad_factor) + \
           (1-less_than_theta)*(y_pred-theta + less_loss_bias)
    return loss

# unary loass
def inst_unary_loss(gt, pred, weights=None, fg_alpha=1.0, margin=2.0):
    '''
    pred/gt: torch.tensor in shape [bs, 1, ht, wd]
    '''
    loss_0 = adjust_smooth_l1_loss(pred)
    loss_1 = adjust_smooth_l1_loss(F.relu(margin-pred))

    raw_gt = (gt > 0.5).float()
    loss = loss_0 * (1-raw_gt) + loss_1 * raw_gt * fg_alpha
    if weights is not None:
        loss = torch.mul(loss, weights)

    return torch.mean(loss.float())


def inst_l1_loss(pred, l1_loss_thr):
    '''
    pred_logits: torch.tensor in shape [bs, 1, ht, wd]
    '''
    def selective_loss(in_logits):
        return (torch.tensor(0) if len(in_logits) else in_logits.sum())

    # main proc
    raw_logits    = torch.reshape(pred - l1_loss_thr, [-1,])

    large_logits  = torch.masked_select(raw_logits, raw_logits>0.5)
    large_loss    = selective_loss(large_logits.float())

    return large_loss


def loss_pw0_no_saturation(pred_pw, gt_pw, pos_wght=3.):
    diff_fct   = torch.exp(-2*pred_pw + 1) + 3
    diff_pw    = torch.mul(pred_pw-1.0, diff_fct)
    loss       = torch.nn.BCEWithLogitsLoss(pos_weight=pos_wght, reduce=False)(diff_pw, gt_pw)

    return loss

def loss_pw0_saturation(pred_pw, gt_pw, pos_wght=3.):
    diff_pw    = 1 - torch.exp(-2*pred_pw + 1)
    loss       = torch.nn.BCEWithLogitsLoss(pos_weight=pos_wght, reduce=False)(diff_pw, gt_pw)
    return loss


def loss_pw_l1_loss(pred_pw, gt_pw, pos_wght=3., margin=1.0):
    loss_pw_1 = adjust_smooth_l1_loss(F.relu(margin - pred_pw))
    loss_pw_0 = adjust_smooth_l1_loss(pred_pw)
    loss = loss_pw_1*gt_pw*pos_wght + loss_pw_0*(1-gt_pw)

    return loss

def loss_pw_exp0(pred_pw, gt_pw, alpha=5., theta = 1e-2, pos_wght=3.0):
    '''
    @ refering paper 'semantic instance segmentation via deep matric learning
    @param: '
    '''
    diff_pw = torch.clamp(torch.abs(pred_pw), 1e-4, 3.0).float()
    sims_pw = 2.0*(1.0-torch.sigmoid(diff_pw))
    gt_pw   = torch.abs(gt_pw> 0.5).float()

    sim_loss = -(gt_pw*torch.log(1.0-sims_pw)*pos_wght + (1.0-gt_pw)*torch.log(sims_pw))
    return sim_loss


def _sampling_over_objects(gt_1d, sample_num, max_wght=None):
    '''
    @func:
    @params: gt_1d      -- tensor in 1D with integers values. Also it's sure gt_1d has value > 0
             sample_num -- number of pixels to sampled
             max_wght   -- threshold for wght to balance contribution of each objects.
                            It's to avoid gradient explosure.
    '''
    unq, cnt = torch.unique(gt_1d, return_counts=True, sorted=True)
    if(unq[0] ==0):
        unq, cnt = unq[1:], cnt[1:]

    if unq.size(0) ==1:
        idx = (gt_1d==unq[0]).nonzero()
        perm = torch.randperm(idx.size(0))
        eff_idx = idx[perm][:sample_num]

        return eff_idx, None
    else:
        # sampling over each single object
        smp_num = sample_num
        smp_idx,  smp_wght   = [], []
        sort_cnt, sort_inc_k = cnt.sort() # increasing on cnt
        for i, k in enumerate(sort_inc_k):
            # sample ratioly
            smp_size = smp_num // (unq.size(0)-i)
            idx = (gt_1d==unq[k]).nonzero()
            perm = torch.randperm(idx.size(0))
            eff_idx = idx[perm][:smp_size]

            # update
            smp_num = smp_num-eff_idx.size(0)
            smp_idx.append(eff_idx)
            smp_wght.append(eff_idx.size(0)*torch.ones([eff_idx.size(0),1], dtype=torch.float))

        smp_idx  = torch.cat(smp_idx, axis=0)
        smp_wght = torch.cat(smp_wght, axis=0)
        smp_wght = smp_wght.max()/(smp_wght+1.0)
        if max_wght is not None:
            smp_wght[smp_wght>max_wght] = max_wght

        return smp_idx, smp_wght


def inst_pairwise_loss_list(gt, pred, weights=None, pos_wght=3.,
                            loss_type='l1', pw_num=4096, obj_wght=0.0):
    '''
    @func:
    @params: gt / pred / weights: tensor in shape [bs, 1, ht, wd]
             pos_wght -- intra pw is positive
             loss_type -- l1 | DM-exp | saturate-exp
    '''
    all_loss = []
    dbg_pw     = []
    h, w = gt.size(2), gt.size(3)
    for k in range(gt.size(0)):
        # reshape gt, pred, weights to [N]
        rs_pred    = pred[k].view(-1)
        rs_gt      = gt[k].view(-1)
        rs_dbg_pw  = rs_gt*0

        # find non-zeros front pixels from gt, and samples if needed
        idx = (rs_gt>0.5).nonzero()
        if len(idx) ==0:
            dbg_pw.append(rs_dbg_pw.view(-1, 1, h, w))
            continue

        elif len(idx) > pw_num:
            idx, smp_wght =  _sampling_over_objects(rs_gt, pw_num, max_wght=None)
        else:
            smp_wght = None
            pass

        # select specified elements out, in shape [bs, N]
        eff_pred  = rs_pred[idx]
        eff_gt    = rs_gt[idx]
        rs_dbg_pw[idx] = 1

        # compute pairwiseinfo, in shape [bs, N, N]
        pw_pred   = torch.clamp(torch.abs(eff_pred[:,None] - eff_pred[None, :]), 1e-4, 5)
        pw_gt     = (torch.abs(eff_gt[:,None] - eff_gt[None, :])>0.5).float()

        # compute loss
        if loss_type == 'l1':
            loss = loss_pw_l1_loss(pw_pred, pw_gt, pos_wght=pos_wght)
        elif loss_type=='DM-exp':
            loss = loss_pw_exp(pw_pred, pw_gt, pos_wght=pos_wght)
        else:
            print('Not validate yet')
            loss = torch.tensor(0.0, dtype=torch.float)

        # multiply weights if not None
        if weights is not None:
            rs_weights = weights[k].view(-1)
            eff_weight = rs_weights[idx]
            if smp_wght is not None and obj_wght>0:
                eff_weight += (smp_wght*obj_wght)
            rs_dbg_pw[idx]  = (eff_weight>0).float()
            pw_weight = eff_weight[:,None] + eff_weight[None, :]
            loss = torch.sum(torch.mul(loss, pw_weight), dim=-1)

        dbg_pw.append(rs_dbg_pw.view(-1, 1, h, w))
        all_loss.append(loss.float().mean())

    if len(all_loss)> 0:
        return {'loss':torch.stack(all_loss).mean(),
                'debug': torch.cat(dbg_pw, axis=0) }
    else:
        return {'loss':torch.tensor(0.0).cuda(),
                'debug': torch.cat(dbg_pw, axis=0) }


def inst_pairwise_loss_conv(gt, pred, diff_kernel, sum_kernel,
                            weights=None,
                            pos_wght=3.,
                            loss_type='l1'):
    '''
    @func:
    @params: gt / pred: tensor in shape [bs, 1, ht, wd]
             pos_wght -- intra pw is positive
             loss_type -- l1 | DM-exp | saturate-exp
    '''
    #print('values: gt ', gt.unique())
    #print('      pred ', pred.round().unique())

    # compute pairwise temporal result
    pw_pred = compute_pairwise_conv(pred, diff_kernel)
    pw_pred = torch.clamp(torch.abs(pw_pred), 1e-4, 5)

    pw_gt   = compute_pairwise_conv(gt, diff_kernel)
    pw_gt   = (torch.abs(pw_gt)>0.5).float()

    sum_pw    = compute_pairwise_conv(gt, sum_kernel)
    #eff_pw    = ((sum_pw-gt) * gt > 0.5).float()

    if weights is not None:
        pw_weight = compute_pairwise_conv(weights, sum_kernel)
        #pw_weight = torch.mul(pw_weight, eff_pw)

    # compute loss
    if loss_type == 'l1':
        loss = loss_pw_l1_loss(pw_pred, pw_gt, pos_wght=pos_wght)
    elif loss_type=='DM-exp':
        loss = loss_pw_exp(pw_pred, pw_gt, pos_wght=pos_wght)
    else:
        print('Not validate yet')
        loss = torch.tensor(0.0, dtype=torch.float)

    if weights is not None:
        loss = torch.sum(torch.mul(loss, pw_weight), dim=-1)

    return {'loss':torch.mean(loss),
            'debug':gt}


def evaluate_instance_pw(gt, pred, diff_kernel, sum_kernel):
    # unary
    gt_bg   = (gt== 0).float()
    unry_bg = torch.sum(pred*gt_bg) / (torch.sum(gt_bg)+1.0)

    gt_fg = 1 - gt_bg
    unry_fg = torch.sum(torch.clamp(pred, max=1.0)*gt_fg) / (torch.sum(gt_fg)+1.0)

    # pairwise
    pw_pred = compute_pairwise_conv(pred, diff_kernel)
    pw_pred = torch.clamp(torch.abs(pw_pred), max=1.0)

    pw_gt   = compute_pairwise_conv(gt, diff_kernel)
    pw_gt   = (torch.abs(pw_gt)>0.5).float()

    sum_pw  = compute_pairwise_conv(gt, sum_kernel)
    eff_pw  = ((sum_pw-gt) * gt > 0.5).float()
    eff_pw_inter = eff_pw * pw_gt
    eff_pw_intra = eff_pw * (1-pw_gt)

    diff_intra = torch.sum(pw_pred*eff_pw_intra) / (torch.sum(eff_pw_intra)+1.)
    diff_inter = torch.sum(pw_pred*eff_pw_inter) / (torch.sum(eff_pw_inter)+1.)

    return {'unary_bg': unry_bg, 'unary_fg':unry_fg,\
            'pw_inter': diff_inter, 'pw_intra': diff_intra}


def customize_pw_kernel(pw_kernel_size=[63, 193],
                        pw_kernel_cen=12,
                        pw_kernel_dilate=4):
    diff_kernel = construct_pairwise_kernel(pw_kernel_size,
                                            pw_kernel_cen,
                                            pw_kernel_dilate,
                                            do_weight=False)

    sum_kernel= construct_pairwise_kernel(pw_kernel_size,
                                          pw_kernel_cen,
                                          pw_kernel_dilate,
                                          do_weight=True)
    return diff_kernel, sum_kernel


def construct_pairwise_kernel(kernel_size=[63, 193],
                              kernel_cen=12,
                              kernel_dilate=4,
                              do_weight=False):
    #  py_kernel in shape [in_ch, ht, wd, out_ch]
    kernel = create_pairwise_conv_kernel(kernel_size, kernel_cen,
                                          dia_stride=kernel_dilate)
    if do_weight== True:
        # center pixel as 1, neighbour pixel as 1 too.
        kernel[:, kernel_size[0]//2, kernel_size[1]//2, :] = -1
        kernel = -1*kernel

    # tc kernel in [out_ch, in_ch, ht, wd]
    tc_kernel = torch.FloatTensor(kernel)
    tc_kernel = tc_kernel.permute(3, 0, 1, 2)
    return tc_kernel


def compute_pairwise_conv(tensor, kernel):
    """
    @func: compute pairwise relationship
    @param: tensor -- size [bs, 1, ht, wd]
            kernel -- size [ch, 1, kht, kwd]
    """
    kht, kwd = kernel.shape[2], kernel.shape[3]
    pad_ht, pad_wd = kht//2, kwd//2
    padT = torch.nn.ReplicationPad2d([pad_wd, pad_wd, pad_ht, pad_ht])(tensor)

    pw = F.conv2d(padT, kernel)
    assert(pw.shape[2:] == tensor.shape[2:])

    return pw


def inst_dice_loss(gt, pred, num_inst=-1):
    '''
    @Param: pred -- instance map after relu. size [bs, ch, ht, wd]
            gt -- in size [bs, ch, ht, wd]
    :: to be test
    '''
    bs, ch, ht, wd = gt.size()
    pred_0 = pred.reshape([bs, ch, -1])
    gt_0   = gt.reshape([bs, ch, -1])
    gt_1   = gt_0.permute([0, 2, 1])
    assert(pred_0.size()[2] == gt_0.size()[2])

    # size should be [bs, ch, ch]
    intp     = torch.matmul(pred_0, gt_1) + 1e0
    union    = pred_0.sum(axis=2) + gt_1.sum(axis=1) + 1e0
    dice_mat = (2.*intp)/(union+1e-4)

    #
    loss = []
    for c in range(ch if num_inst==-1 else num_inst):
        tmp = dice_mat.reshape([bs, -1])
        maxV, maxIdx = dice_mat.max(1)
        loss.append(maxV.mean())
        row, col = (maxIdx//ch).int(), (maxIdx%ch).int()
        for b in range(bs):
            dice_mat[b][row[b]][:] = 0
            dice_mat[b][:][col[b]] = 0

    return (1.0 - torch.stack(loss)).mean()


def inst_topK_loss(gt, pred, K=-1):
    '''
    @Param: pred -- instance map after relu. size [bs, 1, ht, wd]
            gt -- in size [bs, 1, ht, wd]
    '''
    loss = []
    for gtI, predI in zip(gt, pred):
        if K==-1:
            unq = gtI.unique(return_counts=True)
            maxV, cnt = unq[0][-1], 1 if unq[1].size()[0]==1 else unq[1][1:].min()
        else:
            maxV, cnt = gtI.max(), K

        topK = predI.reshape([1,-1]).topk(cnt)
        diff = torch.clamp(maxV - topK[0], min= 0)
        loss.append(diff.mean())

    return torch.stack(loss).mean()


def inst_regularize_loss(pred, kernel_h, kernel_v, clipVal=0.5):
    '''
    @Param: pred -- instance map after relu. size [bs, 1, ht, wd]
            kernel_h/v -- size [1, 1, 3, 3]
    '''
    loss_h = F.conv2d(pred, kernel_h)
    loss_h = torch.clamp(torch.abs(loss_h), max=clipVal)

    loss_v = F.conv2d(pred, kernel_v)
    loss_v = torch.clamp(torch.abs(loss_v), max=clipVal)

    return loss_h.mean() + loss_v.mean()


def inst_quantization_loss(pred):
    '''
    @Param: pred -- instance map after relu. size [bs, 1, ht, wd]
    '''
    pred_int = torch.ceil(pred)
    diff = torch.abs(pred - pred_int)
    return torch.abs(pred - torch.round(pred)).mean()


if __name__=='__main__':
    # test the loss and evaluation functions
    import numpy as np

    gt = np.zeros([2, 1, 32, 32])
    gt[0, 0, 10:20, 10:20] = 1
    gt[0, 0, 10:20, 20:30] = 2
    gt[0, 0, 20:30, 10:]   = 3
    gt[1, 0, 10:30, 10:30] = 4

    pred = gt

    # torch
    tc_gt = torch.FloatTensor(gt)
    tc_preds = torch.FloatTensor(pred)
    diff_kernel, sum_kernel = customize_pw_kernel()

    if True:
        tc_gt = tc_gt.to("cuda")
        tc_preds = tc_preds.to("cuda")
        diff_kernel = diff_kernel.to("cuda")
        sum_kernel = sum_kernel.to("cuda")

    # compute loss and evaluate
    out = evaluate_instance_pw(tc_gt, tc_preds, diff_kernel, sum_kernel)

    unry_loss =  inst_unary_loss(tc_gt, tc_preds)
    pw_loss = inst_pairwise_loss_conv(tc_gt, tc_preds, diff_kernel, sum_kernel)

    print('evaluation:', out, 'loss: unary = ', unry_loss, 'pw = ', pw_loss)


