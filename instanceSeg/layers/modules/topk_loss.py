"""
@ 20201009:: trained on coco and cityscapes, it cannot help as expected:
     1. find K elements from prediction
       -- given K a number:  adopt the topk function in pyTorch to find the k elements
            that has the topk largest value in the prediction
       -- given K = None, find the instance that has maximum MEAN prediction, find all its pixels
     2. compute the distance of the found pixels to the expected largest label value
     3. Experements observation:
       -- given K a number: it only predict expect large label value on a small number of pixels,
          like a small corner on an object
       -- given K = None, it fails to separate instances then, the whole model collapse.
"""

import torch
from torch import nn

class TopkLoss(nn.Module):
    '''This class compute loss for topK elements on predictions of the network.
       It works for encouraging the network to predict large value when there are a lot of objects in targets.
       It works when prediction of the network has only 1 channel.
    '''
    def __init__(self, K=None):
        super(TopkLoss, self).__init__()
        self.K = K

    @torch.no_grad()
    def find_pred_max_instance(self, pred, target):
        '''
        # find the instance with largest pred-value, and return its size.
        @param: pred -- in size [1, ht, wd]
                target -- in size [ch, ht, wd]
        '''
        target_1D = target.view(target.size(0), -1).float()
        pred_1D   = pred.view(1, -1)
        pred_vals = (target_1D * pred_1D).sum(axis=-1)/(target_1D.sum(axis=-1)+1.0)
        ch = pred_vals.argmax()

        return ch if ch>0 else 1


    def forward(self, preds, targets_onehot):
        '''
        @Param: preds -- instance map after relu. size [bs, 1, ht, wd]
                targets_onehot -- in size [bs, ch, ht, wd]
        '''
        loss = []
        for gtI, predI in zip(targets_onehot, preds):
            maxV = (gtI.argmax(axis=0)).max()+1
            if self.K is None:
                ch = self.find_pred_max_instance(predI, gtI)
                if gtI[ch].sum()>0:
                    diff = torch.clamp(maxV - predI[0][gtI[ch]>0], min=0)
                else:
                    diff = predI[0,0,0]*0.0 # stop gradient
            else:
                topK = predI.view(1,-1).topk(self.K)
                diff = torch.clamp(maxV - topK[0], min=0)

            loss.append(diff.mean())

        return torch.stack(loss).mean()

