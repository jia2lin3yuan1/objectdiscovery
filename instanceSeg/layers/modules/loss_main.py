import numpy as np
import torch
from torch import nn
from torch.nn import functional as F

from layers.modules.classify_loss import CrossEntropyLoss
from layers.modules.iou_loss import IoULoss
from layers.modules.topk_loss import TopkLoss
from layers.modules.instance_loss import BinaryLoss, PermuInvLoss, PermuInvLossDilatedConv
from layers.modules.regularize_loss import QuantityLoss, MumfordShahLoss
from layers.modules.evaluate import Evaluate
from discritizer import Discritizer

class LossEvaluate(nn.Module):
    '''
    compute loss:
        1) binary loss to separate BG and FG
        2) permutation invariant loss to separate different instances and group pixels in one obj.
        3) M-S loss to regularize the segmentation level
        4) if predict in 1 channel, use quantity loss to force predicted value close to integer.
           else, use iou-loss to force predicted value close to 1 on corresponding GT.
    '''
    def __init__(self, config, class_weights=None, ignore_label=[255], fg_stCH=1):
        """
        @Param: config -- loss related configurations
                class_weights -- dict with key is the class label and value is the weight
                ignore_label -- ignore class label
                fg_stCH -- it could > 1 for panoptic segmentation
        """
        super(LossEvaluate, self).__init__()
        self.config = config
        self.fg_stCH = fg_stCH
        self.softmax2d = nn.Softmax2d()
        if class_weights is not None:
            class_weights = [class_weights[ele] for ele in sorted(class_weights.keys()) if ele not in ignore_label]
            class_weights = torch.FloatTensor(class_weights).cuda()


        # loss functions
        self.Binary_loss, self.MS_loss, self.PI_loss = None, None, None
        self.Quanty_loss, self.Topk_loss, self.IoU_loss = None, None, None
        self.CE_loss, self.Evaluate = None, None
        if 'pi_alpha' in self.config and self.config['pi_alpha']>0:
            if self.config['pi_mode'] == 'sample-list':
                self.PI_loss = PermuInvLoss(class_weights=class_weights,
                                            margin=self.config['pi_margin'],
                                            pi_pairs=self.config['pi_smpl_pairs'],
                                            smpl_wght_en=self.config['pi_smpl_wght_en'],
                                            pos_wght=self.config['pi_pos_wght'],
                                            loss_type=self.config['pi_loss_type'],
                                            FG_stCH=fg_stCH)
            else: # dilate-conv
                self.PI_loss = PermuInvLossDilatedConv(cuda=torch.cuda.is_available(),
                                            margin=self.config['pi_margin'],
                                            loss_type=self.config['pi_loss_type'],
                                            pi_ksize=self.config['pi_ksize'],
                                            pi_kcen=self.config['pi_kcen'],
                                            pi_kdilate=self.config['pi_kdilate'])

        if 'binary_alpha' in self.config and self.config['binary_alpha']>0:
            class_weights_binary = None
            if self.config['binary_loss_type'] == 'CE':
                class_weights_binary = torch.ones_like(class_weights[:fg_stCH+1])
                class_weights_binary[-1] = class_weights[fg_stCH:].max()

            self.Binary_loss = BinaryLoss(margin=self.config['binary_margin'],
                                        FG_stCH=fg_stCH,
                                        loss_type=self.config['binary_loss_type'],
                                        weights=class_weights_binary)

        if 'regul_alpha' in self.config and self.config['regul_alpha']>0:
            self.MS_loss = MumfordShahLoss()

        if 'quanty_alpha' in self.config and self.config['quanty_alpha']>0:
            self.Quanty_loss = QuantityLoss()

        if 'topk_alpha' in self.config and self.config['topk_alpha']>0:
            self.Topk_loss = TopkLoss(self.config['topk_K'])

        if 'classify_alpha' in self.config and self.config['classify_alpha']>0:
            class_weights_cls = torch.ones_like(class_weights[fg_stCH-1:])
            class_weights_cls[1:] = class_weights[fg_stCH:]
            self.CE_loss = CrossEntropyLoss(weight=class_weights_cls, reduction='none')

        if self.CE_loss is not None or ('iou_alpha' in self.config and self.config['iou_alpha']>0):
            self.IoU_loss = IoULoss(FG_stCH = fg_stCH)

        if self.CE_loss is not None and 'eval_en' in self.config and self.config['eval_en']:
            self.Evaluate = Evaluate(size_thrs=self.config['eval_size_thrs'],
                                     iou_thr  =self.config['eval_iou_thr'],
                                     eval_classes=self.config['eval_classes'])
        if 'discritizer' in self.config and self.config['discritizer']:
            self.discritizer = Discritizer()


    def stableSoftmax(self, logits):
        max_logits = logits.max(dim=1, keepdim=True)[0]
        max_logits.require_grad = False
        return self.softmax2d(logits - max_logits)


    @torch.no_grad()
    def create_global_slope_plane(self, ht, wd):
        '''
        return a tensor in shape [1, 1, ht, wd] with value = row+col
        '''
        slopeX = np.cumsum(np.ones([ht, wd]), axis=1)
        slopeY = np.cumsum(np.ones([ht, wd]), axis=0)
        plane  = slopeX + slopeY
        return torch.FloatTensor(plane[np.newaxis, np.newaxis, ...])

    def forward(self, preds, targets, pred_logits=None, target_boxes=None):
        ''' Compute loss to train the network and report the evaluation metric
        Params: preds -- list of instance prediction in different scale, from FPN.
                targets -- tensor in [bs, ch, ht, wd] with full GT objects.
                pred_logits -- list of classify prediction in different scale, from FPN,
                                in size [bs, N], need to reshape to [bs, ch, cls]
                target_boxes -- list of GT object bboxes with [x0,y0,x1,y1,cls_id-1]
        '''
        bs, ch, _, _ = preds[0].size()
        target_ids = torch.zeros(bs, targets.size(1)-1, dtype=torch.int)
        for b in range(bs):
            target_ids[b] = target_boxes[b][:,-1].int()

        for k in range(len(preds)):
            if pred_logits is not None:
                tmp_ret = self.process_onescale(preds[k], targets, pred_logits[k], target_ids)
            else:
                tmp_ret = self.process_onescale(preds[k], targets, None, target_ids)

            if k == 0:
                ret = tmp_ret
                ret['preds'] = preds[0]
                _, ret['gts']= targets[:,:-1,:,:].max(axis=1, keepdim=True)
                #ret['gts'] = targets[:, 0, :, :]
                ret['wghts'] = targets[:,-1:,:,:]
            else:
                for key in tmp_ret:
                    ret[key] += tmp_ret[key]

        return ret

    @torch.no_grad()
    def resize_GT(self, targets, nht, nwd):
        """
        @Func: resize GT make sure BG channels has no overlap 1.
             if bg_ch == 1, perform bilinear intepolation on each channel, and >0.5 to obtain binary mask
             if bg_ch > 1, perform nearest intepolation on BG channels by combining bg channels into one channel
                           perform bilinear intepolation on FG channels.
        """
        if self.fg_stCH == 1:
            gts_rs  = F.interpolate(targets, size=[nht, nwd], mode='bilinear', align_corners=True)
        else:
            _, targets_bg = targets[:, :-1, :, :].max(axis=1, keepdim=True)
            targets_bg[targets_bg >=self.fg_stCH] = self.fg_stCH
            gts_rs_bg  = F.interpolate(targets_bg.float(), size=[nht, nwd], mode='nearest') # [bs,ch, ht, wd]
            gts_rs_bg  = torch.eye(self.fg_stCH+1)[gts_rs_bg[:,0,:,:].long(), :] # [bs, ht, wd, ch]
            gts_rs_bg = gts_rs_bg.permute([0,3,1,2])[:, :self.fg_stCH, :, :] #[bs, ch, ht, wd]

            targets_fg = targets[:, self.fg_stCH:, :, :]
            gts_rs_fg  = F.interpolate(targets_fg,
                                       size=[nht, nwd],
                                       mode='bilinear', align_corners=True)
            gts_rs = torch.cat([gts_rs_bg, gts_rs_fg], axis=1)

        return gts_rs

    def construct_classify_GT(self, pred_logits, target_ids,
                                    map_indices, map_iou,
                                    pred_mask_probs, entity_prob_thr=0.1, remove_thr=0.9):
        '''
        @Func:  extract prediction and its corresponding targets for compute classify loss
                here class_id recount from 1 for all FG categories.
        @Param:
                pred_logits -- tensor in size [bs, ch, num_classes],
                target_ids -- tensor in size [bs, ch], original input target classIds
                map_indices -- tensor in size [bs, 2, ch], with [0] for pred, [1] for GT
                map_iou -- tensor in size [bs, ch]
                pred_mask_probs -- tensor in size [bs, ch, ht, wd],
        @Output:
                preds_cls -- list of tensor in size [num_classes]
                gts_cls -- list of class_id for train classify net. ref IoU
        '''
        bs, ch, num_classes = pred_logits.size()
        pred_mask_maxProb   = torch.max(pred_mask_probs.view(bs, ch, -1), axis=-1)[0] #[bs, ch]

        preds_cls, gts_cls  = [], []
        for b in range(bs):
            for k in range(self.fg_stCH, ch):
                pj, gj = map_indices[b][0][k], map_indices[b][1][k]

                # remove empty prediction with a ratio 'remove_thr'
                if pred_mask_maxProb[b, pj] < entity_prob_thr and torch.rand(1) < remove_thr:
                    continue
                else:
                    preds_cls.append(pred_logits[b, pj])
                    sem_id = target_ids[b][gj]-self.fg_stCH+1
                    iou    = map_iou[b][k]
                    if iou >= self.config['classify_pos_iou_thr']:
                        gts_cls.append(sem_id)
                    else:
                        gts_cls.append(sem_id * 0)

        return preds_cls, gts_cls


    def process_onescale(self, preds, targets, pred_logits=None, target_ids=None):
        ''' Compute loss to train the network and report the evaluation metric
        Params: preds -- tensor for instance prediction.
                targets -- tensor in [bs, ch, ht, wd] with full GT objects.
                pred_logits -- classify prediction, in size [bs, N], reshape to [bs, ch, cls]
                target_ids -- tensor in [bs, ch], GT categoryID for each target
        '''
        bs, ch, ht, wd = preds.size()
        # prepare data
        #gts_rs  = F.interpolate(targets, size=[ht, wd], mode='bilinear', align_corners=True)
        gts_rs    = self.resize_GT(targets, ht, wd)
        if False:
            from matplotlib import pyplot as plt
            _, ta = preds.max(axis=1)
            _, tb = targets[:, :-1, :, :].max(axis=1)
            _, tc = gts_rs[:, :-1, :, :].max(axis=1)
            fig, ax = plt.subplots(1,3)
            ax[0].imshow(ta[-1].cpu().detach().numpy())
            ax[1].imshow(tb[-1].cpu().detach().numpy())
            ax[2].imshow(tc[-1].cpu().detach().numpy())
            plt.show()
            import pdb; pdb.set_trace()

        weights_0 = gts_rs[:, -1:, :, :]
        if ch > 1:
            assert(ch<=targets.size(1)-1)
            preds_0 = self.softmax2d(preds) if self.config['use_softmax'] else preds
            gts_0 = (gts_rs[:, :ch, :, :]>0.5).int() # larger objects have smaller channel ID
            target_ids = target_ids[:, :ch]
        else:
            assert(pred_logits is None)
            preds_0 = preds
            _, gts_0 = gts_rs[:, :-1, :, :].max(axis=1, keepdim=True)
            gts_onehot = (gts_rs[:, :(gts_0.max()+1), :, :]>0.5).int()

            if 'glb_trend_en' in self.config and self.config['glb_trend_en']==1:
                plain = self.create_global_slope_plane(ht, wd)
                preds = preds+plain.cuda()
        if pred_logits is not None:
            pred_logits = pred_logits.view(bs, ch, -1)

        # compute loss 1
        ret = {}
        ret['preds_0'] = preds_0

        if self.Binary_loss is not None:
            loss = self.Binary_loss(preds_0, gts_0, weights=weights_0)
            ret['binary'] = loss * self.config['binary_alpha']

        # process on network output after binary loss
        if ch==1:
            preds_0 = F.relu(preds_0)
        elif ch > 1 and not self.config['use_softmax']:
            preds_0 = self.softmax2d(preds)

        if ch==1 and self.Quanty_loss is not None:
            loss = self.Quanty_loss(preds_0)
            ret['quanty'] = loss * self.config['quanty_alpha']

        if ch==1 and self.Topk_loss is not None:
            loss = self.Topk_loss(preds_0, gts_onehot)
            ret['topk'] = loss * self.config['topk_alpha']

        if self.MS_loss is not None:
            loss = self.MS_loss(preds_0)
            ret['regul'] = loss * self.config['regul_alpha']

        if self.PI_loss is not None:
            if self.config['pi_mode'] == 'sample-list':
                loss = self.PI_loss(preds_0, gts_0,
                                    target_ids=target_ids,
                                    weights= None if self.config['pi_smpl_wght_en']>0 else weights_0,
                                    BG=self.config['pi_hasBG'])
            else:
                loss = self.PI_loss(preds_0, gts_0.float(),
                                    weights= None if self.config['pi_smpl_wght_en']>0 else weights_0,
                                    BG=self.config['pi_hasBG'])
            if loss is not None:
                ret['pi'] = loss['loss'] * self.config['pi_alpha']
                ret['eval_pi0'] = loss['eval_pi0']
                ret['eval_pi1'] = loss['eval_pi1']

        preds_rmEdge   = preds_0*((weights_0>0).float()) # remove edge garbage from batch resize
        if self.IoU_loss is not None and ch > 1:
            loss = self.IoU_loss(preds_rmEdge, gts_0, return_hungarian_map=False)
            iou_iou, iou_indices =loss['iou-iou'], loss['indices']
            ret['iou'] = loss['loss'] * self.config['iou_alpha']

        if self.CE_loss is not None and pred_logits is not None and 'iou' in ret:
            preds_cls, gts_cls = self.construct_classify_GT(pred_logits, target_ids,
                                                            iou_indices, iou_iou, preds_0)
            if len(preds_cls) > 0:
                preds_cls = torch.stack(preds_cls)
                gts_cls   = torch.stack(gts_cls).view(-1)
                loss      = self.CE_loss(preds_cls, gts_cls.long())
                ret['classify'] = loss.mean() * self.config['classify_alpha']

        if self.Evaluate is not None and self.CE_loss is not None:
            target_cls_ids = target_ids - self.fg_stCH + 1
            target_cls_ids[target_cls_ids< 0] = 0
            evalV = self.Evaluate(preds_rmEdge, gts_0, pred_logits, target_cls_ids)
            ret['eval_prec'] = evalV['prec']
            ret['eval_rec'] = evalV['rec']
            ret['eval_acc'] = evalV['acc']

        if ch==1 and self.discritizer is not None and False:
            pred_labels = self.discritizer(preds)

        return ret
